.PHONY: build
build:
	poetry run $(MAKE) run
.PHONY: run
run: test
.PHONY: test
test:
	pytest
